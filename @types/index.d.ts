interface Config {
  logging: {
    debug: boolean;
    logRocket: boolean;
    why: boolean;
  }
}

declare global {
  interface Window {
    CONFIG: Config
  }
}

export {}
