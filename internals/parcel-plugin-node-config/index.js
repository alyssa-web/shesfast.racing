const fs = require('fs')
const cheerio = require('cheerio')
const config = require('config');

/**
 *
 * @see https://github.com/parcel-bundler/parcel/packages/core/parcel-bundler/src/Bundler.js
 *
 * bundler.entryFiles
 * bundler.options
 * bundler.resolver
 * bundler.parser
 * bundler.packagers
 * bundler.cache
 * bundler.delegate
 * bundler.bundleLoaders
 * bundler.pending
 * bundler.loadedAssets
 * bundler.watchedAssets
 * bundler.farm
 * bundler.watcher
 * bundler.hmr
 * bundler.bundleHashes
 * bundler.error
 * bundler.buildQueue
 * bundler.rebuildTimeout
 * bundler.server
 *
 * bundler.on('buildStart')
 * bundler.on('bundled')
 * bundler.on('buildError')
 * bundler.on('buildEnd')
 */

module.exports = function(bundler) {
  // console.warn(bundler)
  // bundler.addAssetType('ext', require.resolve('./MyAsset'))
  // bundler.addPackager('foo', require.resolve('./MyPackager'))

  // console.warn(config)
  // console.warn(config.util)
  // bundler.options.global = { CONFIG: JSON.stringify(config) }

  // bundler.on('buildStart', console.warn) // bundler.options.entryFiles
  // bundler.on('bundled', console.warn)
  // bundler.on('buildError', console.error)
  // bundler.on('buildEnd', () => console.warn('done'))

  bundler.on('bundled', bundle => {
    const bundles = Array.from(bundle.childBundles).concat([bundle]);
      // console.warn(bundles)

      // find bundle.type === 'js' and get .name for insertBefore

    return Promise.all(bundles.map(async bundle => {
      if (!bundle.entryAsset || bundle.entryAsset.type !== "html") return;

      const cwd = bundle.entryAsset.options.outDir;
      const content = fs.readFileSync(bundle.name, 'utf-8');
      const $ = cheerio.load(content)

      $('#CONFIG').remove()
      $(`<script id="CONFIG" type="text/javascript"> const CONFIG = ${JSON.stringify(config)} </script>`).insertBefore('script')

      fs.writeFileSync(bundle.name, $.html())
    }));
  })
}

/**
 * @ref https://github.com/janouma/parcel-plugin-text/blob/master/src/index.js
 * @ref https://github.com/krotovic/parcel-plugin-interpolate-html/blob/master/src/asset.js
 * @ref https://github.com/shff/parcel-plugin-inliner/blob/master/index.js
 * @ref https://github.com/Joe-Withey/parcel-plugin-html-root-syntax/blob/master/index.js
 */
