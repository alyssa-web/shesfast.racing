# ShesFast Racing

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/alyssa-web/shesfast.racing)

## Getting Started

`$ yarn`

### Configuration

- `LOGROCKET`
- `DEBUG` - Log Level "Verbose" must be set in Browser console

see
- https://github.com/lorenwest/node-config/wiki/Environment-Variables
- https://www.npmjs.com/package/debug#environment-variables

### Prerequisites

1. AWS
    - set `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`
1. Serverless
    - set `SERVERLESS_ACCESS_KEY` to log in without a browser

## Develop

`$ yarn start`

### Typescript

### React/Redux

### Styles

## Test

`$ yarn test`

## Deploy

This app is a [Serverless Component](https://github.com/serverless-components).

`$ yarn deploy`

### Debugging

LogRocket
