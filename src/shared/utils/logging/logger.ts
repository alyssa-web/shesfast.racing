import debug from 'debug';

/** __DEBUG__.enable('NAMESPACE'), or __DEBUG__.disable('NAMESPACE') */
// @ts-ignore
if (CONFIG.logging.debug) window.__DEBUG__ = debug;

export const log = debug('app') // or 'client'
