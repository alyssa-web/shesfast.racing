import LogRocket from 'logrocket';

export const initLogRocket = () => {
  LogRocket.init(`${process.env.LOGROCKET_BASE}/shesfast`);
}
