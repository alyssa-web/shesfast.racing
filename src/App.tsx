import React, { useEffect, useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Grid, Paper, Link, Typography, ImageList, ImageListItem, useMediaQuery } from '@material-ui/core';
import { ThemeProvider, createTheme } from '@material-ui/core/styles';

import './shared/styles/main.scss';

const ACCESS_TOKEN =
  'IGQVJYSFpKQjZAKTEh6dy1GTDBtQlhUY2FVRGVTcWNmb1JEQzE1bjZAYckRvaXBmZAEdGLU5CeE45dVBDLUQ3MWQ0cVNQTW5ZAZAzdhc0RZARHZAsOHRrUDBlRmZA2TnNveTZAveW03WWVfeVVpY1U3cHQ1SGNfVAZDZD';

const AFTER = new Date('2020-09-01')

// #FF6700 - Orange
// #FFDE2B - Yellow
// #5E00FF - Blue
// #FF2CD2 - Pink

const darkTheme = createTheme({
  palette: {
    type: 'dark',
    background: {
      paper: '#111',
    },
    primary: {
      main: '#FFDE2B', // yellow
    },
    secondary: {
      main: '#FF6700', // orange
    },
    // text: {}
  },
});

export const App = () => {
  // const [paging, setPaging] = useState({});
  const [feed, updateFeed] = useState([]);
  const matchesXs = useMediaQuery(darkTheme.breakpoints.only('xs'));
  const matchesSm = useMediaQuery(darkTheme.breakpoints.only('sm'));
  const matchesMd = useMediaQuery(darkTheme.breakpoints.only('md'));
  const matchesLg = useMediaQuery(darkTheme.breakpoints.only('lg'));
  const matchesXl = useMediaQuery(darkTheme.breakpoints.only('xl'));

  useEffect(() => {
    fetch(`https://graph.instagram.com/me/media?fields=id,media_type,media_url,timestamp&access_token=${ACCESS_TOKEN}`)
      .then((response) => response.json())
      .then(({ data, paging }) => {
        // setPaging(paging);
        updateFeed(data.filter(({ timestamp }) => new Date(timestamp) > AFTER ));
      });
  }, []);

  let rowHeight

  if(matchesXs) {
    rowHeight = 100
  } else if (matchesSm) {
    rowHeight = 180
  } else if (matchesMd) {
    rowHeight = 120
  } else if (matchesLg) {
    rowHeight = 160
  } else if (matchesXl) {
    rowHeight = 400
  }

  return (
    <ThemeProvider theme={darkTheme}>
      <Grid container component="main" className="root">
        <CssBaseline />
        <Grid item xs={12} sm={12} md={7} className="image" />
        <Grid item xs={12} sm={12} md={5} component={Paper} elevation={6} square>
          <div className="paper">
            <Typography component="h1" variant="h5">
              Alyssa Evans
            </Typography>
            <Typography>CVMA #743</Typography>

            <Typography align="center">2020 Femmewalla Champion (Alyssa Davis #704)</Typography>

            <Typography>2020 Kawasaki ZX-6R</Typography>

            <dl></dl>

            {/* <Link href="https://www.hookit.com/members/alyda/" target="_blank" rel="noopener">Results on HookIt</Link> */}
            <Link href="https://www.instagram.com/shesfast_racing/" target="_blank" rel="noopener">
              shesfast_racing is on Instagram
            </Link>

            {/* <Typography>Sponsors</Typography> */}

            {/* <Grid container item className="feed">
              {feed.map(({ id }) => <Grid item key={id}>{media_url}</Grid>)}
            </Grid> */}

            <ImageList component="section" className="feed" cols={3} rowHeight={rowHeight}>
              {feed.map(({ id, media_url }) => (
                <ImageListItem key={id} cols={1}>
                  <img src={media_url} />
                </ImageListItem>
              ))}
            </ImageList>
          </div>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
};

export { App as default };
