# Custom Domain

## Registrar (NameCheap)

### NameServers

Custom DNS

## AWS

### Route53

1. Create a hosted zone
1. Set Custom NameServers
1. Add A Records pointing to Cloudfront
1. Add A Record Alias for www -> root

### ACM

1. Add CNAME records to validate

### Cloudfront

1. Set Alternate Domain Names (CNAMEs) (www and root)
1. Select Custom SSL Certificate
