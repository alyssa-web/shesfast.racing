// To use an ENV VAR as a boolean use this `json` utility
function json(env) {
  return {
    __name: env,
    __format: 'json',
  };
}

module.exports = {
  logging: {
    debug: 'DEBUG',
    logRocket: json('LOGROCKET'),
    why: json('WHY')
  }
}
